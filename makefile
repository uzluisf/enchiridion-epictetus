CMD_SEP = "========================================"
CMD_DONE = "Done."
CLEAN_CMD = rm -f *.aux       ; \
            rm -f *.log       ; \
            rm -f *.out       ; \
            rm -f *.ilg       ; \
            rm -f *.idx       ; \
            rm -f *.ent       ; \
            rm -f *.toc       ; \
            rm -f *.ind       ; \

all: full

full:
	@echo "FULL BUILD"
	@echo
	@echo $(CMD_SEP)
	@echo "Building epictetus-enchiridion (1st time)..."
	pdflatex -jobname=epictetus-enchiridion main.tex
	@echo $(CMD_DONE)
	@echo $(CMD_SEP)
	@echo
	@echo $(CMD_SEP)
	pdflatex -jobname=epictetus-enchiridion main.tex
	@echo $(CMD_DONE)
	@echo $(CMD_SEP)
	@echo
	@echo $(CMD_SEP)
	@echo "Rebuilding epictetus-enchiridion (3rd time)..."
	pdflatex -jobname=epictetus-enchiridion main.tex
	@echo $(CMD_DONE)
	@echo $(CMD_DONE)
	@echo $(CMD_SEP)
	@echo
	@echo $(CMD_SEP)
	@echo "Rebuilding epictetus-enchiridion (4th time)..."
	pdflatex -jobname=epictetus-enchiridion main.tex
	@echo $(CMD_DONE)
	@echo $(CMD_SEP)
	@echo
	@echo $(CMD_SEP)
	@echo "Rebuilding epictetus-enchiridion (last time)..."
	pdflatex -jobname=epictetus-enchiridion main.tex
	@echo $(CMD_DONE)
	@echo $(CMD_SEP)
	@echo

clean:
	@echo $(CMD_SEP)
	@echo "Cleaning '.' folder..."
	@-$(CLEAN_CMD)
	@echo $(CMD_DONE)
	@echo $(CMD_SEP)
	@echo
